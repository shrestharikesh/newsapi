<?php

namespace Tests\Feature;

use App\Models\Impression;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ImpressionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_an_impression_is_recorded()
    {
        $user = User::factory()->create();
        $url = 'https://www.bbc.co.uk/news/uk-58417078';


        $response = $this->actingAs($user, 'api')->json('POST', '/api/toggle-impression', [
            'impression' => 'like',
            'url' => $url
        ], [
            'Accept' => 'application/json'
        ]);
        $this->assertCount(1, Impression::all());
        $response->assertStatus(200);
    }
    /** @test */
    public function test_an_impression_reverted()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $url = 'https://www.bbc.co.uk/news/uk-58417078';
        Impression::create([
            'user_id' => $user->id,
            'news_url' => $url,
            'impression' => 'like'
        ]);

        $response = $this->actingAs($user, 'api')->json('POST', '/api/toggle-impression', [
            'impression' => 'like',
            'news_url' => $url
        ], [
            'Accept' => 'application/json'
        ]);
        $this->assertCount(0, Impression::all());
        $response->assertStatus(200);
    }


    /** @test */
    public function test_user_impression_can_be_fetched()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $url = 'https://www.bbc.co.uk/news/uk-58417078';
        

        $response = $this->actingAs($user, 'api')->json('POST', '/api/user-impression', [
            'news_url' => $url
        ], [
            'Accept' => 'application/json'
        ]);
        // $this->assertCount(0, Impression::all());
        $response->assertStatus(200);
    }
}
