<?php

namespace Tests\Feature;

use App\Models\User;
use DateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function all_fields_are_required_during_registration()
    {
        $response =  $this->json('POST', '/api/auth/register', [
            'name' => '',
            'email' => '',
            'password' => '',
            'password_confirmation' => 'password',
        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(422)->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "name" => ["The name field is required."],
                "email" => ["The email field is required."],
                "password" => ["The password field is required."],
            ]
        ]);
    }

    /** @test */
    public function password_should_be_confirmed_during_registration()
    {
        $response =  $this->json('POST', '/api/auth/register', [
            'name' => 'Rikesh Shrestha',
            'email' => 'rikesh@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'abcd',
        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(422)->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "password" => ["The password confirmation does not match."],
            ]
        ]);
    }


    /** @test */
    public function test_user_can_be_created_through_registration()
    {
        $this->withoutExceptionHandling();
        $this->post('/api/auth/register', [
            'name' => 'Rikesh Shrestha',
            'email' => 'rikesh@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ], [
            'Accept' => 'application/json'
        ])->assertStatus(201);

        $this->assertCount(1, User::all());
    }


    /** @test */
    public function test_user_can_login()
    {
        $this->withoutExceptionHandling();
        $this->generatePersonalAccessClient();

        $user = User::factory()->create();
        $respose = $this->json('POST','/api/auth/login', [
            'email' => $user->email,
            'password' => 'password',
            'remember_me' => true
        ], [
            'Accept' => 'application/json'
        ]);

        $respose->assertStatus(200)
            ->assertJsonStructure(['access_token', 'token_type', 'expires_at']);
    }
    /** @test */
    public function test_User_logout()
    {
        $this->withoutExceptionHandling();
        $this->generatePersonalAccessClient();
        $user = User::factory()->create();
        $token = $user->createToken('Personal Access Token')->accessToken;
        $this->json('GET','/api/auth/logout',[], ['Accept' => 'application/json', 'Authorization' => 'Bearer ' . $token])
            ->assertStatus(200);
    }

    private function generatePersonalAccessClient()
    {
        $clientRepository = new ClientRepository();
        $client = $clientRepository->createPersonalAccessClient(
            null,
            'Test Personal Access Client',
            env('APP_URL')
        );

        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->id,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);
    }
}
