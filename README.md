Installation Instruction


Install composer from [here](https://getcomposer.org/Composer-Setup.exe).

Install Git bash from [here](https://git-scm.com/download/win)

Clone the project by running command on git bash:
``` bash
git clone https://shrestharikesh@bitbucket.org/shrestharikesh/newsapi.git
```
cd into the project :
``` bash
cd newsapi
```
Run the following command to install composer:

``` bash
composer install
```
Generate .env by running following command :
``` bash
cp .env.example .env
```
Create a database 

Open .env and fill database_name, username and password of your mysql server
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=newsapi
DB_USERNAME=root
DB_PASSWORD=
```
Open .env and fill NEWS_API key generated from [newsapi.org](https://newsapi.org/) at the end
```bash
NEWS_API=
```
Finally, run following command and you are good to go:
``` bash
php artisan key:generate
php artisan migrate
php artisan passport:install
php artisan serve
```
you can view the website on <http://localhost:8000>




