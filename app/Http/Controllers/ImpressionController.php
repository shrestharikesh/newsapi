<?php

namespace App\Http\Controllers;

use App\Models\Impression;
use Illuminate\Http\Request;

class ImpressionController extends Controller
{
    public function toggleImpression(Request $request)
    {
        $impression = Impression::where('user_id', $request->user()->id)
            ->where('news_url', 'LIKE', $request->url)
            ->first();
        if ($impression) {
            $impression->delete();
            if ($impression->impression == $request->impression)
                return response()->json([
                    'message' => "Impression removed.",
                    "success" => true
                ]);
        }

        Impression::create([
            'user_id' => $request->user()->id,
            'impression' => $request->impression,
            'news_url' => $request->url
        ]);
        return response()->json([
            'message' => "Impression created successfully.",
            "success" => true
        ]);
    }
    public function getCount(Request $request)
    {
        $url = $request->url;
        $like = Impression::where('news_url', 'LIKE', $url)->where('impression', 'like')->count();
        $dislike = Impression::where('news_url', 'LIKE', $url)->where('impression', 'dislike')->count();
        return  response()->json([
            "count" => ['like' => $like, 'dislike' => $dislike]
        ]);
    }

    public function getUserImpression(Request $request)
    {
        $impression = Impression::where('news_url','LIKE',$request->url)->where('user_id',$request->user()->id)->first();
        return response()->json([
            'impression'=> $impression ? $impression->impression :''
        ]);
    }
}
