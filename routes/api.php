<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ImpressionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class,'login']);
    Route::post('register', [AuthController::class,'register']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', [AuthController::class,'logout']);
        Route::post('toggle-impression', [ImpressionController::class,'toggleImpression']);
    });
});
Route::post('impression-count',[ImpressionController::class,'getCount']);
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('toggle-impression', [ImpressionController::class,'toggleImpression']);
    Route::post('user-impression', [ImpressionController::class,'getUserImpression']);
});
